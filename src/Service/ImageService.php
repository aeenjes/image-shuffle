<?php


namespace App\Service;

use App\Entity\Image;
use App\Repository\ImageRepository;

class ImageService
{
    /** @var ImageRepository */
    private $imageRepository;

    /**
     * ImageService constructor.
     * @param ImageRepository $imageRepository
     */
    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    public function getRandomImage()
    {
        /** Hier haal ik alle afbeeldingen op */
        $images = $this->imageRepository->findAll();

        /** Hier tel ik alle afbeeldingen bij elkaar op */
        $count = count($images);
        if (!$count) {
            return false;
        }

        /** Hier return ik een random afbeelding van de reeks afbeeldingen die hierboven opgehaald zijn
         *  Dit doe ik door een random getal in de array te plaatsen
         *  aan de hand van hoeveel afbeeldingen er opgehaald zijn
         */
        return $images[rand(0, $count - 1)];
    }

    public function getAllImages()
    {
        /** Hier haal ik alle afbeeldingen op */
        return $this->imageRepository->findAll();
    }

    public function createImage(Image $image)
    {
        /** Hier wordt er een nieuwe afbeelding aangemaakt */
        $this->imageRepository->create($image);
    }

    public function getImageById(Image $image)
    {
        /** Hier haal ik een afbeelding op aan de hand van een ID */
        return $this->imageRepository->findOneBy(['id' => $image->getId()]);
    }

    public function updateImage(Image $image)
    {
        /** Hier update ik een afbeelding aan de hand van de opgehaalde afbeelding */
        $this->imageRepository->update($image);
    }

    public function deleteImage(Image $image)
    {
        /** Hier verwijder ik een afbeelding aan de hand van de opgehaalde afbeelding */
        $this->imageRepository->delete($image);
    }
}