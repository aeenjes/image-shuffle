<?php

namespace App\Controller;

use App\Entity\Image;
use App\Form\ImageType;
use App\Service\ImageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class AppController extends AbstractController
{
    /** @var ImageService */
    private $imageService;

    /**
     * AppController constructor.
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function indexAction()
    {
        /** Hier haal ik een random afbeelding op */
        $image = $this->imageService->getRandomImage();

        /** Hier wordt er gevalideerd of er een afbeelding beschikbaar is, zo niet
         *  gaat de code hierdoorheen en geeft hij een flash message terug met dat
         *  er nog een afbeelding gemaakt moet worden.
         */
        if(!$image) {
            $this->addFlash('warning', 'Oops! There is no image, please create first an image!');
        }

        return $this->render('index.html.twig', ['image' => $image]);
    }

    public function overviewAction()
    {
        /** Hier haal ik alle afbeeldingen op */
        $images = $this->imageService->getAllImages();

        return $this->render('overview.html.twig', ['images' => $images]);
    }

    public function createAction(Request $request)
    {
        /** Hier definieer ik een nieuwe afbeelding */
        $image = new Image();

        /** Hier creëer ik een formulier die getoond gaat worden in de frontend */
        $form = $this->createForm(ImageType::class, $image);

        $form->handleRequest($request);

        /** Hier valideer ik of het formulier verzonden en valide is */
        if ($form->isSubmitted() && $form->isValid()) {

            /** Zodra het formulier verzonden en valide is, maak ik een nieuwe afbeelding aan
             *  Aan de hand van de ingevulde informatie in het formulier
             */
            $this->imageService->createImage($image);

            /** Hier maak ik een nieuwe flash message aan, wat getoond wordt in de frontend */
            $this->addFlash(
                'success',
                'You successfully created an image!'
            );

            /** Zodra er een nieuwe afbeelding is gemaakt en de flash message is klaargezet
             *  redirect ik de pagina naar de overview pagina
             */
            return $this->redirectToRoute('overview');
        }

        return $this->render('create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function editAction(Request $request, Image $image)
    {
        /** Hier haal ik een afbeelding op aan de hand van een ID */
        $this->imageService->getImageById($image);

        $form = $this->createForm(ImageType::class, $image);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->imageService->updateImage($image);

            $this->addFlash(
                'success',
                'You successfully updated an image!'
            );

            return $this->redirectToRoute('overview');
        }

        return $this->render('edit.html.twig', [
            'form' => $form->createView(),
            'image' => $image
        ]);
    }

    public function deleteAction(Image $image)
    {
        /** Hier verwijder ik een afbeelding aan de hand van de opgehaalde afbeelding */
        $this->imageService->deleteImage($image);

        $this->addFlash(
            'danger',
            'You successfully deleted an image!'
        );

        return $this->redirectToRoute('overview');
    }
}