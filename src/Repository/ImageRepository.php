<?php

namespace App\Repository;

use App\Entity\Image;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Image|null find($id, $lockMode = null, $lockVersion = null)
 * @method Image|null findOneBy(array $criteria, array $orderBy = null)
 * @method Image[]    findAll()
 * @method Image[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Image::class);
    }

    public function create(Image $image)
    {
        /** Hier wordt daadwerkelijk de afbeelding aangemaakt */
        $this->getEntityManager()->persist($image);
        $this->getEntityManager()->flush();
    }

    public function update(Image $image)
    {
        /** Hier wordt daadwerkelijk de afbeelding geupdate */
        $this->getEntityManager()->merge($image);
        $this->getEntityManager()->flush();
    }

    public function delete(Image $image)
    {
        /** Hier wordt daadwerkelijk de afbeelding verwijderd */
        $this->getEntityManager()->remove($image);
        $this->getEntityManager()->flush();
    }
}
